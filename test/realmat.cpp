#include "common.h"
#include "fast_matrix_market/app/triplet.hpp"
#include "parallel.h"
#include "timer.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <random>
#include <set>

void print_help() {
    std::cout
        << "Usage: mpi_benchmark <file> <col_send_strategy> "
           "<request_column_strategy> <final_gather> <finegrained>"
           "files: <ubuntu> 10e5 600'000 nz, <poli> 10e4 40'000 nz, <major> 10e5 1'700'000 nz"
           "<lhr> 10e4 400'000 nz, <cube> 10e6 124'000'000 nz, <webbase> 10e8 1'000'000'000 nz"
           "<road> 10e6 5'000'000 nz, <mavi> 10e8 270'000'000 nz, <uk> 10e7 300'000'000 nz"
           "<germany> 10e7 25'000'000"
        << std::endl;
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    if (argc != 6 || std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help") {
        print_help();
        return 1;
    } else {

        bool finalgather = false;
        if (std::string(argv[4]) == "true") {
            finalgather = true;
        }

        bool finegrain = false;
        if (std::string(argv[5]) == "true") {
            finegrain = true;
        }

        ColSendStrategy col_send_strategy;
        if (std::string(argv[2]) == "alltoall") {
            col_send_strategy = ColSendStrategy::ALL_GATHER;
        } else if (std::string(argv[2]) == "broadcast") {
            col_send_strategy = ColSendStrategy::BROADCAST;
        } else if (std::string(argv[2]) == "optimized_alltoall") {
            col_send_strategy = ColSendStrategy::OPTIMIZED_ALL_TO_ALL;
        } else {
            std::cerr << "Error: Invalid column sending strategy." << std::endl;
            std::cerr << "Valid column sending strategies: alltoall, broadcast, optimized_alltoall"
                      << std::endl;
            return 1;
        }

        RequestedColStrategy requested_col_strategy;
        if (std::string(argv[3]) == "merged_vector") {
            requested_col_strategy = RequestedColStrategy::MERGED_VECTOR;
        } else if (std::string(argv[3]) == "merged_set") {
            requested_col_strategy = RequestedColStrategy::MERGED_SET;
        } else if (std::string(argv[3]) == "bloom_filter") {
            requested_col_strategy = RequestedColStrategy::BLOOM_FILTER;
        } else {
            std::cerr << "Error: Invalid requesting columns strategy." << std::endl;
            std::cerr
                << "Valid requesting columns strategies: merged_vector, merged_set, bloom_filter"
                << std::endl;
            return 1;
        }

        auto matrix_file_path = "../test/matrices/oscil_dcop_11.mtx";

        if (std::string(argv[1]) == "ubuntu") {
            matrix_file_path = "../../test/matrices/sx-askubuntu.mtx";
        } else if (std::string(argv[1]) == "webbase") {
            matrix_file_path = "../../test/matrices/webbase-2001/webbase-2001.mtx";
        } else if (std::string(argv[1]) == "poli") {
            matrix_file_path = "../../test/matrices/poli3.mtx";
        } else if (std::string(argv[1]) == "lhr") {
            matrix_file_path = "../../test/matrices/lhr17.mtx";
        } else if (std::string(argv[1]) == "road") {
            matrix_file_path = "../../test/matrices/roadNet-CA.mtx";
        } else if (std::string(argv[1]) == "cube") {
            matrix_file_path = "../../test/matrices/Cube_Coup_dt6.mtx";
        } else if (std::string(argv[1]) == "rgg") {
            matrix_file_path = "../../test/matrices/rgg_n_2_24_s0.mtx";
        } else if (std::string(argv[1]) == "mavi") {
            matrix_file_path = "../../test/matrices/mawi_201512020130.mtx";
        } else if (std::string(argv[1]) == "germany") {
            matrix_file_path = "../../test/matrices/germany_osm.mtx";
        } else if (std::string(argv[1]) == "major") {
            matrix_file_path = "../../test/matrices/majorbasis.mtx";
        } else {
            std::cerr << "Error: Invalid file." << std::endl;
            std::cerr
                << "Valid files: ubuntu, poli, lhr, mavi, road, cube, major, webbase, uk, germany"
                << std::endl;
            return 1;
        }

        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        if (rank == 0) {

            struct triplet_matrix {
                int64_t nrows = 0, ncols = 0;
                std::vector<int64_t> rows, cols;
                std::vector<double> vals; // or int64_t, float, std::complex<double>, etc.
            } mat;

            std::ifstream matrix_file(matrix_file_path);
            fast_matrix_market::read_matrix_market_triplet(matrix_file, mat.nrows, mat.ncols,
                                                           mat.rows, mat.cols, mat.vals);

            COOMatrix triplets;
            for (size_t i = 0; i < mat.rows.size(); ++i) {
                triplets.push_back(Triplet(mat.rows[i], mat.cols[i], mat.vals[i]));
            }

            CRSMatrix A = coo_to_crs(triplets, mat.nrows, mat.ncols);
            CCSMatrix B = coo_to_ccs(triplets, mat.nrows, mat.ncols);

            Timer timer;
            timer.start();
            parallel_symbolic_spgemm(A, B, col_send_strategy, requested_col_strategy,
                                     MPI_COMM_WORLD, finalgather, finegrain);
            timer.stop();
            std::cout << "Total runtime: " << timer.duration() << " seconds" << std::endl;

        } else {
            parallel_symbolic_spgemm(std::nullopt, std::nullopt, col_send_strategy,
                                     requested_col_strategy, MPI_COMM_WORLD, finalgather,
                                     finegrain);
        }

        MPI_Finalize();
    }
}
