#include <fast_matrix_market/app/Eigen.hpp>
#include <fstream>
#include <gtest/gtest.h>

#include "fast_matrix_market/app/array.hpp"
#include "sequential.h"

int main(int argc, char *argv[]) {
    int result = 0;

    ::testing::InitGoogleTest(&argc, argv);
    result = RUN_ALL_TESTS();

    return result;
}

TEST(SequentialTest, SequentialRamu) {
    auto matrix_file_path = "../test/matrices/oscil_dcop_11.mtx";

    Eigen::SparseMatrix<double> A_Eigen;
    Eigen::SparseMatrix<double> B_Eigen;

    std::ifstream matrix_file(matrix_file_path);
    fast_matrix_market::read_matrix_market_eigen(matrix_file, A_Eigen);
    B_Eigen = A_Eigen;

    Eigen::SparseMatrix<double> C_Eigen = A_Eigen * B_Eigen;

    CRSMatrix A = readMatrixFromMatrixMarketCRS(matrix_file_path);
    CCSMatrix B = readMatrixFromMatrixMarketCCS(matrix_file_path);

    auto C = sequential_spgemm(A, B);

    Eigen::SparseMatrix<double> C_(C_Eigen.rows(), C_Eigen.cols());
    std::vector<Eigen::Triplet<double>> tripletList;
    for (auto &triplet : C) {
        tripletList.push_back(Eigen::Triplet<double>(triplet.i, triplet.j, triplet.val));
    }

    C_.setFromTriplets(tripletList.begin(), tripletList.end());
    ASSERT_TRUE(C_Eigen.isApprox(C_));
}
