#include "common.h"
#include "timer.h"
#include <Eigen/Sparse>
#include <fast_matrix_market/app/Eigen.hpp>
#include <iostream>
#include <fstream>
#include <vector>

void print_help() {
    std::cout << "Usage: eigen_benchmark <matrix_rows> <matrix_cols> or eigen_benchmark "
                 "<matrix_file_path>"
              << std::endl;
}

int main(int argc, char *argv[]) {

    if ((argc != 2 && argc != 3) || std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help") {
        print_help();
        return 1;
    } else {
        Eigen::SparseMatrix<double> A_Eigen;
        Eigen::SparseMatrix<double> B_Eigen;

        if (argc == 3) {
            size_t rows = std::stoi(argv[1]);
            size_t cols = std::stoi(argv[2]);
            double p = 1e-4;

            A_Eigen.resize(rows, cols);
            B_Eigen.resize(rows, cols);

            std::cout << "rows: " << rows << " cols: " << cols << std::endl;

            if (rows <= 0 || cols <= 0) {
                std::cerr << "Error: The matrix dimensions must be positive." << std::endl;
                return 1;
            }

            COOMatrix A = generate_random_matrix(rows, cols, p);
            COOMatrix B = generate_random_matrix(rows, cols, p);

            std::vector<Eigen::Triplet<double>> A_Eigen_Triplets;
            for (auto &triplet : A) {
                A_Eigen_Triplets.push_back(
                    Eigen::Triplet<double>(triplet.i, triplet.j, triplet.val));
            }

            std::vector<Eigen::Triplet<double>> B_Eigen_Triplets;
            for (auto &triplet : B) {
                B_Eigen_Triplets.push_back(
                    Eigen::Triplet<double>(triplet.i, triplet.j, triplet.val));
            }

            A_Eigen.setFromTriplets(A_Eigen_Triplets.begin(), A_Eigen_Triplets.end());
            B_Eigen.setFromTriplets(B_Eigen_Triplets.begin(), B_Eigen_Triplets.end());
        } else {
            std::ifstream matrix_file(argv[1]);
            fast_matrix_market::read_matrix_market_eigen(matrix_file, A_Eigen);
            B_Eigen = A_Eigen;
        }

        Timer timer;
        timer.start();

        Eigen::SparseMatrix<double> C_Eigen = A_Eigen * B_Eigen;

        timer.stop();
        std::cout << "Total runtime: " << timer.duration() << " seconds" << std::endl;
    }
}
