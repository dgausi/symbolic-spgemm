#include "common.h"
#include "parallel.h"
#include <fast_matrix_market/app/Eigen.hpp>
#include <fstream>
#include <gtest/gtest.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
    int result = 0;

    MPI_Init(&argc, &argv);

    ::testing::InitGoogleTest(&argc, argv);
    result = RUN_ALL_TESTS();

    MPI_Finalize();

    return result;
}

std::vector<Eigen::Triplet<double>> to_triplets(Eigen::SparseMatrix<double> &M) {
    std::vector<Eigen::Triplet<double>> v;
    for (int i = 0; i < M.outerSize(); i++)
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(M, i); it; ++it)
            v.emplace_back(it.row(), it.col(), it.value());
    return v;
}

TEST(MPITest, MPITest) {
    int rows = 10000;
    int cols = 10000;
    double p = 10e-6;

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    COOMatrix A = generate_random_matrix(rows, cols, p);
    COOMatrix B = generate_random_matrix(rows, cols, p);

    Eigen::SparseMatrix<double> A_Eigen(rows, cols);
    Eigen::SparseMatrix<double> B_Eigen(rows, cols);

    std::vector<Eigen::Triplet<double>> A_Eigen_Triplets;
    for (auto &triplet : A) {
        A_Eigen_Triplets.push_back(Eigen::Triplet<double>(triplet.i, triplet.j, triplet.val));
    }

    std::vector<Eigen::Triplet<double>> B_Eigen_Triplets;
    for (auto &triplet : B) {
        B_Eigen_Triplets.push_back(Eigen::Triplet<double>(triplet.i, triplet.j, triplet.val));
    }

    A_Eigen.setFromTriplets(A_Eigen_Triplets.begin(), A_Eigen_Triplets.end());
    B_Eigen.setFromTriplets(B_Eigen_Triplets.begin(), B_Eigen_Triplets.end());

    Eigen::SparseMatrix<double> C_Eigen = A_Eigen * B_Eigen;

    std::vector<ColSendStrategy> col_send_strategies = {ColSendStrategy::BROADCAST,
                                                        ColSendStrategy::ALL_GATHER,
                                                        ColSendStrategy::OPTIMIZED_ALL_TO_ALL};

    std::vector<RequestedColStrategy> requested_col_strategies = {
        RequestedColStrategy::MERGED_VECTOR, RequestedColStrategy::MERGED_SET,
        RequestedColStrategy::BLOOM_FILTER};

    for (const auto &col_send_strategy : col_send_strategies) {
        for (const auto &requested_col_strategy : requested_col_strategies) {

            if (rank == 0) {
                std::cout << std::endl;
                std::cout << "Col send strategy: " << col_send_strategy
                          << ", Requested col strategy: " << requested_col_strategy << std::endl;

                auto C = parallel_symbolic_spgemm(
                             coo_to_crs(A, rows, cols), coo_to_ccs(B, rows, cols),
                             col_send_strategy, requested_col_strategy, MPI_COMM_WORLD, true, false)
                             .value();

                // Check number of nnz match
                EXPECT_TRUE(static_cast<size_t>(C_Eigen.nonZeros()) == C.size())
                    << "Eigen: " << C_Eigen.nonZeros() << ", C: " << C.size()
                    << ", col_send_strategy: " << col_send_strategy
                    << ", requested_col_strategy: " << requested_col_strategy;

                // Check each nnz
                for (auto &triplet : C) {
                    EXPECT_TRUE(C_Eigen.coeff(triplet.i, triplet.j) != 0.0)
                        << "i = " << triplet.i << ", j = " << triplet.j
                        << " col_send_strategy: " << col_send_strategy
                        << ", requested_col_strategy: " << requested_col_strategy;
                }
            } else {
                parallel_symbolic_spgemm(std::nullopt, std::nullopt, col_send_strategy,
                                         requested_col_strategy, MPI_COMM_WORLD, true, false);
            }

            MPI_Barrier(MPI_COMM_WORLD);

            if (col_send_strategy != ColSendStrategy::OPTIMIZED_ALL_TO_ALL) {
                break;
            }
        }
    }
}
