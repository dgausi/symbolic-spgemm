#include "common.h"
#include "parallel.h"
#include "timer.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <random>
#include <set>

void print_help() {
    std::cout << "Usage: mpi_benchmark <matrix_rows> <matrix_cols> <col_send_strategy> "
                 "<request_column_strategy> <final_gather> <finegrained>"
              << std::endl;
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    if (argc != 7 || std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help") {
        print_help();
        return 1;
    } else {
        size_t rows = std::stoi(argv[1]);
        size_t cols = std::stoi(argv[2]);
        double p = 1e-4;

        bool finalgather = false;
        if (std::string(argv[5]) == "true") {
            finalgather = true;
        }

        bool finegrain = false;
        if (std::string(argv[6]) == "true") {
            finegrain = true;
        }

        ColSendStrategy col_send_strategy;
        if (std::string(argv[3]) == "alltoall") {
            col_send_strategy = ColSendStrategy::ALL_GATHER;
        } else if (std::string(argv[3]) == "broadcast") {
            col_send_strategy = ColSendStrategy::BROADCAST;
        } else if (std::string(argv[3]) == "optimized_alltoall") {
            col_send_strategy = ColSendStrategy::OPTIMIZED_ALL_TO_ALL;
        } else {
            std::cerr << "Error: Invalid column sending strategy." << std::endl;
            std::cerr << "Valid column sending strategies: alltoall, broadcast, optimized_alltoall"
                      << std::endl;
            return 1;
        }

        RequestedColStrategy requested_col_strategy;
        if (std::string(argv[4]) == "merged_vector") {
            requested_col_strategy = RequestedColStrategy::MERGED_VECTOR;
        } else if (std::string(argv[4]) == "merged_set") {
            requested_col_strategy = RequestedColStrategy::MERGED_SET;
        } else if (std::string(argv[4]) == "bloom_filter") {
            requested_col_strategy = RequestedColStrategy::BLOOM_FILTER;
        } else {
            std::cerr << "Error: Invalid requesting columns strategy." << std::endl;
            std::cerr
                << "Valid requesting columns strategies: merged_vector, merged_set, bloom_filter"
                << std::endl;
            return 1;
        }

        if (rows <= 0 || cols <= 0) {
            std::cerr << "Error: The matrix dimensions must be positive." << std::endl;
            return 1;
        }

        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        if (rank == 0) {

            auto A = generate_random_matrix_crs(rows, cols, p);
            auto B = generate_random_matrix_ccs(rows, cols, p);

            Timer timer;
            timer.start();

            parallel_symbolic_spgemm(A, B, col_send_strategy, requested_col_strategy,
                                     MPI_COMM_WORLD, finalgather, finegrain);
            MPI_Barrier(MPI_COMM_WORLD);

            timer.stop();
            std::cout << "Total runtime: " << timer.duration() << " seconds" << std::endl;

        } else {
            parallel_symbolic_spgemm(std::nullopt, std::nullopt, col_send_strategy,
                                     requested_col_strategy, MPI_COMM_WORLD, finalgather,
                                     finegrain);
            MPI_Barrier(MPI_COMM_WORLD);
        }

        MPI_Finalize();
    }
}
