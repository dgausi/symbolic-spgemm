// first attempt of Sparse matrix multiplication (sequantial approach)
// A*B=C where we have A stored in CRS
// Created by ramu on 27.10.23.
//

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "common.h"

COOMatrix sequential_spgemm(const CRSMatrix &A, const CCSMatrix &B) {

    // printCRSMatrix(crs_A);
    // printCCSMatrix(ccs_B);

    int N = A.num_rows;
    int M = B.num_cols;

    assert(A.num_cols == B.num_rows);

    COOMatrix C;

    // COMPUTATION OF A*B
    // PRE: A must be CRS and B CCS format
    // POST: C in COO format

    for (int i = 0; i < N; ++i) {     // Iterate over rows of A
        for (int j = 0; j < M; ++j) { // Iterate over columns of B
            double sum = 0.0;

            // Get the range of non-zero values for row i in A
            int start_A = A.row_ptr[i];
            int end_A = A.row_ptr[i + 1];

            // Get the range of non-zero values for column j in B
            int start_B = B.col_ptr[j];
            int end_B = B.col_ptr[j + 1];

            for (int index_A = start_A; index_A < end_A; ++index_A) {
                int col_A = A.columns[index_A];
                for (int index_B = start_B; index_B < end_B; ++index_B) {
                    int row_B = B.rows[index_B];
                    if (col_A == row_B) {
                        sum += A.values[index_A] * B.values[index_B];
                        break; // No need to continue searching for matching indices in B
                    }
                }
            }

            if (sum != 0.0) {
                Triplet T(i, j, sum);
                C.push_back(T);
            }
        }
    }

    // printAsMatrix(C,N,M);

    return C;
}
