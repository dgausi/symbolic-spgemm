#include "parallel.h"
#include "common.h"
#include "hash.h"
#include "timer.h"
#include <algorithm>
#include <cassert>
#include <climits>
#include <functional>
#include <immintrin.h>
#include <iostream>
#include <map>
#include <mpi.h>
#include <random>
#include <ranges>
#include <set>
#include <span>
#include <unordered_set>

/**
 * struct RowColWrapper - Wrapper for a CRS row or a CCS column
 */
struct RowColWrapper {
    RowColWrapper() = default;

    /**
     * @brief Construct a new RowColWrapper from a CRS row or a CCS column.
     *
     * @param[in] idx The index of the row or column.
     * @param[in] size The number of elements in the row or column.
     * @param[in] indices The indices of the elements in the row or column.
     */
    RowColWrapper(int idx, std::vector<int> indices) : indices(std::move(indices)), idx(idx) {}

    static std::unordered_map<int, RowColWrapper> parse_recvbuffer(std::vector<int> recvbuffer) {
        std::unordered_map<int, RowColWrapper> res;

        for (size_t i = 0; i < recvbuffer.size(); i += recvbuffer[i] + 2) {
            auto &size = recvbuffer[i];
            auto &idx = recvbuffer[i + 1];

            assert(size > 0);

            RowColWrapper row_col(
                idx, std::vector<int>(std::make_move_iterator(recvbuffer.begin() + i + 2),
                                      std::make_move_iterator(recvbuffer.begin() + i + 2 + size)));
            res[row_col.idx] = row_col;
        }

        return res;
    }

    int size() const { return indices.size(); }

    int total_size() const { return size() + 2; }

    std::vector<int> indices;
    int idx;
};

/**
 * @brief Calculates displacement vector based on the counts vector.
 *
 * @param[in] counts The number of elements to be sent to each process.
 * @return displs The displacement vector.
 */
inline std::vector<int> calc_contiguous_displs(const std::vector<int> &counts) {
    std::vector<int> displs(counts.size());
    for (size_t i = 0; i < displs.size(); i++) {
        displs[i] = i == 0 ? 0 : displs[i - 1] + counts[i - 1];
    }

    return displs;
}

/**
 * @brief Check if the indices of the row and column overlap.
 *
 * @param[in] row Vector containing the indices of the items in the row.
 * @param[in] col Vector containing the indices of the items in the column.
 * @return True if there is an overlap, false otherwise.
 * @warning The indices of row and column must be sorted.
 */
inline bool has_overlap(const std::vector<int> &row, const std::vector<int> &col) {

    // Sanitize inputs
    if (!row.size() || !col.size()) {
        return false;
    }

    size_t i = 0;
    size_t j = 0;

    do {
        if (row[i] == col[j]) {
            return true;
        } else if (row[i] < col[j]) {
            ++i;
        } else {
            ++j;
        }
    } while (i < row.size() && j < col.size());
    return false;
}

/**
 * @brief Scatter the rows or columns of a CRS or CCS matrix.
 *
 * @param[in] send_row_cols The rows or columns to be sent.
 * @param[in] num_workers The number of workers.
 * @param[in] num_per_rank The number of rows or columns to be sent to each worker.
 * @param[in] num_total The total number of rows or columns.
 * @param[in] rank The rank of the current process.
 * @param[in] communicator The MPI communicator.
 * @return Local rows or columns assigned to the current process.
 */
inline std::unordered_map<int, RowColWrapper>
scatter_row_col(const std::vector<RowColWrapper> &send_row_cols, int num_workers, int num_per_rank,
                int num_total, int rank, MPI_Comm communicator) {
    // Scatter the number of rows/columns that will be sent to each process
    int num_local;
    std::vector<int> num_each_rank(num_workers, num_per_rank);
    num_each_rank[num_workers - 1] = num_total - (num_workers - 1) * num_per_rank;
    MPI_Scatter(num_each_rank.data(), 1, MPI_INT, &num_local, 1, MPI_INT, 0, communicator);

    // Scatter the row/column sizes
    std::vector<int> size_local(num_local);
    std::vector<int> size;
    if (rank == 0) {
        size.reserve(num_total);
        for (const auto &row_col : send_row_cols) {
            size.push_back(row_col.total_size()); // Send total size to allocate enough memory
        }
    }

    MPI_Scatterv(size.data(), num_each_rank.data(), calc_contiguous_displs(num_each_rank).data(),
                 MPI_INT, size_local.data(), num_local, MPI_INT, 0, communicator);

    // Setup the send buffer
    std::vector<int> sendbuffer_row_cols;
    if (rank == 0) {
        sendbuffer_row_cols.reserve(std::reduce(size.begin(), size.end(), 0, std::plus<int>()));
        for (auto &row : send_row_cols) {
            sendbuffer_row_cols.push_back(row.size());
            sendbuffer_row_cols.push_back(row.idx);
            for (const auto &idx : row.indices) {
                sendbuffer_row_cols.push_back(idx);
            }
        }
    }

    // Calculate the sendcounts for each process
    std::vector<int> sendcounts(num_workers);
    if (rank == 0) {
        for (int i = 0; i < num_workers; i++) {
            for (int j = 0; j < num_each_rank[i]; j++) {
                sendcounts[i] += size[i * num_per_rank + j];
            }
        }
    }

    // Setup the buffer to receive the assigned rows
    int recvcount_local = std::reduce(size_local.begin(), size_local.end(), 0, std::plus<int>());
    std::vector<int> recvbuffer(recvcount_local);

    MPI_Scatterv(sendbuffer_row_cols.data(), sendcounts.data(),
                 calc_contiguous_displs(sendcounts).data(), MPI_INT, recvbuffer.data(),
                 recvcount_local, MPI_INT, 0, communicator);

    std::unordered_map<int, RowColWrapper> local_row_cols =
        RowColWrapper::parse_recvbuffer(recvbuffer);
    assert(local_row_cols.size() == (size_t)num_local);

    return local_row_cols;
}

/**
 * @brief For the given local rows, generates a merged row (either set or vector) and shares this to
 * all other processors and stores the received merged_rows
 */
inline std::vector<std::vector<int>>
compute_and_share_merged_row(const std::unordered_map<int, RowColWrapper> &local_rows,
                             RequestedColStrategy requested_col_strategy, int num_workers,
                             MPI_Comm communicator) {
    // Merge the rows
    std::set<int> merged_set;
    std::vector<int> merged_row;

    if (requested_col_strategy == RequestedColStrategy::MERGED_VECTOR) {
        for (const auto &[row_idx, row] : local_rows) {
            merged_row.insert(merged_row.end(), row.indices.begin(), row.indices.end());
        }

        std::sort(merged_row.begin(), merged_row.end());
    }

    if (requested_col_strategy == RequestedColStrategy::MERGED_SET) {
        // Currently we merge rows via inserting into a set (which auto-sorts in C++)
        for (const auto &[row_idx, row] : local_rows) {
            for (auto element : row.indices) {
                merged_set.insert(element);
            }
        }

        // Assign values from the set to the vector
        merged_row.assign(merged_set.begin(), merged_set.end());
    }

    int send_size = merged_row.size();
   // std::cout << "send size: " << send_size
    //          << std::endl; // INEFFICIENT: These are huge (e.g. 152, 163, 169)

    // Communicate the send sizes to all processes
    std::vector<int> recv_counts(num_workers);
    MPI_Allgather(&send_size, 1, MPI_INT, recv_counts.data(), 1, MPI_INT, communicator);

    // Allocate the receive buffer
    int recv_size = std::reduce(recv_counts.begin(), recv_counts.end());
    std::vector<int> recv_buffer(recv_size);

    // Send the merged row to all other processes. Merged_row size varies, thus Allgatherv is needed
    MPI_Allgatherv(merged_row.data(), send_size, MPI_INT, recv_buffer.data(), recv_counts.data(),
                   calc_contiguous_displs(recv_counts).data(), MPI_INT, communicator);

    // Parse the recv_buffer into a vector of merged_rows
    std::vector<std::vector<int>> merged_rows;
    merged_rows.reserve(num_workers);

    int offset = 0;
    for (int i = 0; i < num_workers; ++i) {
        int count = recv_counts[i]; // Number of elements received from worker i

        // Create a vector with elements from recv_buffer[offset] to recv_buffer[offset + count]
        std::vector<int> workerData(recv_buffer.begin() + offset,
                                    recv_buffer.begin() + offset + count);

        // Add this vector to the outer vector
        merged_rows.push_back(workerData);

        // Update the offset for the next iteration
        offset += count;
    }

    // DEBUG: Checked gathered merged_row sizes
    //for (auto merged_row : merged_rows) {
    //    std::cout << "merged row size: " << merged_row.size() << std::endl;
    //}

    // Return a vector containing the merged_rows of all other processors
    return merged_rows;
}

/**
 * @brief For the given merged_rows, computes which local columns are required by each processor by
 * checking overlap between local cols and merged row
 */
inline std::vector<std::vector<int>>
calc_send_cols(const std::unordered_map<int, RowColWrapper> &local_cols,
               const std::vector<std::vector<int>> &merged_rows, int num_workers) {

    std::vector<std::vector<int>> all_send_cols;
    all_send_cols.reserve(num_workers);

    // Given the local_cols and merged_rows, find the overlaps and store the required columns
    for (int i = 0; i < num_workers; i++) {
        auto merged_row = merged_rows[i];

        std::vector<int> required_cols;

        for (const auto &[col_idx, col] : local_cols) {
            if (has_overlap(col.indices, merged_row)) {
                required_cols.push_back(col_idx);
            }
        }

        all_send_cols.push_back(required_cols);
    }

    // Return a vector containing all the columns to send per processor
    return all_send_cols;
}

/**
 * @brief Generates a bloom filter for the local rows and shares this to all other processors and
 * stores the received bloom_filters
 */
inline std::vector<std::vector<char>>
compute_and_share_bloom_filter(const std::unordered_map<int, RowColWrapper> &local_rows,
                               int num_workers, MPI_Comm communicator) {

    // Calc the size of the merged_set to set the bloomfilter size <- probably more efficient,
    // probabilistic way
    std::unordered_set<int> merged_set;

    for (const auto &[row_idx, row] : local_rows) {
        for (auto element : row.indices) {
            merged_set.insert(element);
        }
    }

    int n = merged_set.size();
    int m = 16 * n; // size of the std::vector<bool> , size of the i-th bloom filter

    std::vector<char> charArray(m / 8, 0);

    for (int i : merged_set) {
        charArray.at(hashFunction1(i, m) / 8) |= (1 << (hashFunction1(i, m) % 8));
        charArray.at(hashFunction2(i, m) / 8) |= (1 << (hashFunction2(i, m) % 8));
        charArray.at(hashFunction3(i, m) / 8) |= (1 << (hashFunction3(i, m) % 8));
        charArray.at(hashFunction4(i, m) / 8) |= (1 << (hashFunction4(i, m) % 8));
        charArray.at(hashFunction5(i, m) / 8) |= (1 << (hashFunction5(i, m) % 8));
        charArray.at(hashFunction6(i, m) / 8) |= (1 << (hashFunction6(i, m) % 8));
        charArray.at(hashFunction7(i, m) / 8) |= (1 << (hashFunction7(i, m) % 8));
        charArray.at(hashFunction8(i, m) / 8) |= (1 << (hashFunction8(i, m) % 8));
        charArray.at(hashFunction9(i, m) / 8) |= (1 << (hashFunction9(i, m) % 8));
        charArray.at(hashFunction10(i, m) / 8) |= (1 << (hashFunction10(i, m) % 8));
        charArray.at(hashFunction11(i, m) / 8) |= (1 << (hashFunction11(i, m) % 8));
    }

    int sendSize = charArray.size();

    // Communicate the send sizes to all processes with MPI_Allgather
    std::vector<int> recv_counts(num_workers);
    MPI_Allgather(&sendSize, 1, MPI_INT, recv_counts.data(), 1, MPI_INT, communicator);

    // Allocate the receive buffer
    int recv_size = std::reduce(recv_counts.begin(), recv_counts.end());
    std::vector<char> recv_buffer(recv_size);

    // Send the merged row to all other processes. Merged_row size varies, thus Allgatherv is needed
    MPI_Allgatherv(charArray.data(), sendSize, MPI_CHAR, recv_buffer.data(), recv_counts.data(),
                   calc_contiguous_displs(recv_counts).data(), MPI_CHAR, communicator);

    std::vector<std::vector<char>> bloom_filters;

    // Parse the recv_buffer into a vector of bloom_filters (of type <std::vector<char>>)
    int offset = 0;
    for (int i = 0; i < num_workers; ++i) {
        int count = recv_counts[i]; // Number of elements received from worker i

        // Create a vector with elements from recv_buffer[offset] to recv_buffer[offset + count]
        std::vector<char> workerData(recv_buffer.begin() + offset,
                                     recv_buffer.begin() + offset + count);

        // Add this vector to the outer vector
        bloom_filters.push_back(workerData);

        // Update the offset for the next iteration
        offset += count;
    }

    return bloom_filters;
}

/**
 * @brief For the given bloom_filters, computes which local columns are required by each processor
 * by checking hashes between local cols and bloom filters
 */
inline std::vector<std::vector<int>>
calc_send_cols_bloom(std::vector<std::vector<char>> bloomfilters,
                     const std::unordered_map<int, RowColWrapper> &local_cols, int num_workers) {
    std::vector<std::vector<int>> all_send_cols(num_workers);

    // Iterate through each bloomfilter and check which cols are required
    for (int i = 0; i < num_workers; i++) {
        auto bloomfilter = bloomfilters[i];
        int m = bloomfilter.size() * 8; // Convert charArray size to bit size

        if (!m) {
            return all_send_cols;
        }

        std::vector<int> required_cols;

        for (const auto &[col_idx, col] : local_cols) {
            for (auto element : col.indices) {
                bool present = true;
                // Check all hash functions
                std::initializer_list<std::function<int(int, int)>> hashFunctions = {
                    hashFunction1, hashFunction2,  hashFunction3, hashFunction4,
                    hashFunction5, hashFunction6,  hashFunction7, hashFunction8,
                    hashFunction9, hashFunction10, hashFunction11};
                for (auto &hashFunc : hashFunctions) {
                    int bitIndex = hashFunc(element, m);
                    int charIndex = bitIndex / 8;
                    int bitOffset = bitIndex % 8;

                    if (!(bloomfilter[charIndex] & (1 << bitOffset))) {
                        present = false;
                        break;
                    }
                }
                if (present) {
                    required_cols.push_back(col_idx);
                    break;
                }
            }
        }

        all_send_cols[i] = required_cols;
    }

    return all_send_cols;
}

/**
 * @brief Send/receive columns to/from other processes with optimized Alltoall.
 *
 * @param[in] local_cols Columns of the matrix that are assigned to the current process.
 * @param[in] send_cols Array storing which col_idx of current process are required by other columns
 * @param[in] num_workers The number of workers.
 * @param[in] communicator The MPI communicator.
 */
inline std::unordered_map<int, RowColWrapper>
send_recv_cols_alltoall(std::unordered_map<int, RowColWrapper> &local_cols,
                        const std::vector<std::vector<int>> &send_cols, int num_workers,
                        MPI_Comm communicator) {

    // Populate the send_buffer with the required send_cols
    std::vector<int> send_buffer;
    std::vector<int> send_counts(num_workers, 0);

    for (auto i = 0; i < num_workers; i++) {
        const auto columns_to_send = send_cols[i];

        for (auto col_idx : columns_to_send) {
            const auto col = local_cols[col_idx];
            send_buffer.push_back(col.size());
            send_buffer.push_back(col_idx);
            send_buffer.insert(send_buffer.end(), col.indices.begin(), col.indices.end());
            send_counts[i] += col.total_size();
        }
    }

    // Calculate the size required for the receive_buffer -> Get recv counts from each processor via
    // AllToAll
    std::vector<int> recv_counts(num_workers);
    MPI_Alltoall(send_counts.data(), 1, MPI_INT, recv_counts.data(), 1, MPI_INT, communicator);
    int recv_size = std::reduce(recv_counts.begin(), recv_counts.end());

    std::vector<int> recv_buffer(recv_size);

    // Share all the columns to all the other processors
    MPI_Alltoallv(send_buffer.data(), send_counts.data(),
                  calc_contiguous_displs(send_counts).data(), MPI_INT, recv_buffer.data(),
                  recv_counts.data(), calc_contiguous_displs(recv_counts).data(), MPI_INT,
                  communicator);

    // Parse the recvbuffer to get the columns
    std::unordered_map<int, RowColWrapper> received_cols =
        RowColWrapper::parse_recvbuffer(recv_buffer);

    return received_cols;
}

/**
 * @brief Send/receive all local columns to/from other processes with allGather.
 *
 * @param[in] local_cols Columns of the matrix that are assigned to the current process.
 * @param[in] num_workers The number of workers.
 * @param[in] communicator The MPI communicator.
 */
inline std::unordered_map<int, RowColWrapper>
send_recv_cols_allgather(const std::unordered_map<int, RowColWrapper> &local_cols, int num_workers,
                         MPI_Comm communicator, bool finegrained, int rank) {
    Timer ata_send_timer;
    if (finegrained) {
        // times send of all to all unoptimized
        ata_send_timer.start();
    }

    // Calculate size to allocate for the send buffer
    int send_size = 0;
    for (const auto &[col_idx, col] : local_cols) {
        send_size += col.total_size();
    }

    // Populate the send_buffer with the local cols
    std::vector<int> send_buffer;
    send_buffer.reserve(send_size);

    // TODO: Is there a more efficent way for this? -> memory (rebuild recvbuffer) or efficiency
    // (store recvbuffer)?
    for (const auto &[col_idx, col] : local_cols) {
        send_buffer.push_back(col.size());
        send_buffer.push_back(col_idx);
        send_buffer.insert(send_buffer.end(), col.indices.begin(), col.indices.end());
    }

    // Share the send_buffer sizes to all other processes
    std::vector<int> recv_counts(num_workers);
    // We need to know recv_size from each processor to compute displs -> MPI_Allgather (not
    // MPI_Allreduce)
    MPI_Allgather(&send_size, 1, MPI_INT, recv_counts.data(), 1, MPI_INT, communicator);
    int recv_size = std::reduce(recv_counts.begin(), recv_counts.end());

    //std::cout << "recv counts: ";
    //for (auto e : recv_counts) {
    //    std::cout << e << " ";
    //}
    //std::cout << std::endl;

    if (finegrained) {
        ata_send_timer.stop();
        std::cout << "Rank " << rank << ": all to all send time: " << ata_send_timer.duration()
                  << std::endl;
    }

    Timer ata_recv_timer;
    if (finegrained) {
        // times send of all to all unoptimized
        ata_recv_timer.start();
    }

    // Allocate the recv_buffer()
    std::vector<int> recv_buffer(recv_size);

    // Send the local_cols to all other processes -> fixed cols send to all other proccesses ->
    // MPI_Allgatherv (not MPI_Alltoall)
    MPI_Allgatherv(send_buffer.data(), send_size, MPI_INT, recv_buffer.data(), recv_counts.data(),
                   calc_contiguous_displs(recv_counts).data(), MPI_INT, communicator);

    // Parse the recvbuffer to get the columns
    std::unordered_map<int, RowColWrapper> received_cols =
        RowColWrapper::parse_recvbuffer(recv_buffer);

    if (finegrained) {
        ata_recv_timer.stop();
        std::cout << "Rank " << rank << ": all to all receive time: " << ata_recv_timer.duration()
                  << std::endl;
    }

    return received_cols;
}

/**
 * @brief Compute the non-zeroes broadcasting the columns of B.
 *
 * @param[in] local_rows Rows of the matrix that are assigned to the current process.
 * @param[in] local_cols Columns of the matrix that are assigned to the current process.
 * @param[in] col_assignment The column assignment vector.
 * @param[in] cols_per_rank The number of columns assigned to each process.
 * @param[in] num_workers The number of workers.
 * @param[in] rank The rank of the current process.
 * @param[in] communicator The MPI communicator.
 */
inline std::vector<int>
compute_non_zeroes_broadcast(const std::unordered_map<int, RowColWrapper> &local_rows,
                             const std::unordered_map<int, RowColWrapper> &local_cols,
                             const std::vector<int> &col_assignment, int cols_per_rank,
                             int num_workers, int rank, MPI_Comm communicator) {
    std::vector<int> non_zeroes;

    // Broadcast columns of B
    for (size_t idx = 0; idx < col_assignment.size(); idx++) {
        int col_idx = col_assignment.at(idx);
        int root = idx / cols_per_rank;

        int size = 0;
        if (rank == root) {
            assert(local_cols.contains(col_idx));
            size = local_cols.at(col_idx).size();
        }

        MPI_Bcast(&size, 1, MPI_INT, root, communicator);

        std::vector<int> current_col(size);
        if (rank == root) {
            current_col.assign(local_cols.at(col_idx).indices.begin(),
                               local_cols.at(col_idx).indices.end());
        }

        MPI_Bcast(current_col.data(), size, MPI_INT, root, communicator);

        for (const auto &[row_idx, row] : local_rows) {

            if (has_overlap(row.indices, current_col)) {
                non_zeroes.push_back(row_idx);
                non_zeroes.push_back(col_idx);
            }

            // std::cout << "BROADCAST indices: " << row_idx << "," << col_idx << std::endl;
        }

        MPI_Barrier(communicator);
    }

    return non_zeroes;
}

/**
 * @brief Compute the non-zeroes with the two pointers method or hash_table
 * @param[in] local_rows Rows of the matrix that are assigned to the current process.
 * @param[in] local_cols Columns of the matrix that are assigned to the current process.
 * @param[in] use_two_pointers If true, use the two pointers method O(2^n^3), otherwise use
 * hash_table O(n^3)
 */
inline std::vector<int> compute_non_zeros(const std::unordered_map<int, RowColWrapper> &local_rows,
                                          const std::unordered_map<int, RowColWrapper> &local_cols,
                                          bool use_two_pointers) {

    // TODO: Tighter bound
    size_t pre_allocation_size = 2 * std::max(local_rows.size(), local_cols.size());
    //std::cout << "non_zeroes pre-allocated size: " << pre_allocation_size << std::endl;

    std::vector<int> non_zeroes;
    non_zeroes.reserve(pre_allocation_size);

    if (use_two_pointers) {
        // Use 2 pointers -> O(2*n^3)
        for (const auto &[row_idx, row] : local_rows) {
            for (const auto &[col_idx, col] : local_cols) {
                if (has_overlap(row.indices, col.indices)) {
                    non_zeroes.push_back(row_idx);
                    non_zeroes.push_back(col_idx);
                }
            }
        }

        //std::cout << "non_zeroes actual size: " << non_zeroes.size() << std::endl;
    } else {

        std::unordered_map<int, std::vector<int>> col_map; // value, col_idx
        for (const auto &[col_idx, col] : local_cols) {
            for (auto element : col.indices) {
                col_map[element].push_back(col_idx); // TODO: Pre-allocate col_map[element]
            }
        }

        for (const auto &[row_idx, row] : local_rows) {
            for (auto element : row.indices) {
                if (col_map.find(element) != col_map.end()) {
                    for (auto col_idx : col_map[element]) {
                        non_zeroes.push_back(row_idx);
                        non_zeroes.push_back(col_idx);
                    }
                }
            }
        }

        // for (const auto &[col_idx, col] : local_cols) {
        // std::unordered_map<int, int> col_map; // value, col_idx
        // for (auto element : col.indices) {
        // col_map[element] = col_idx;
        // }

        // for (const auto &[row_idx, row] : local_rows) {
        // for (auto element : row.indices) {
        // if (col_map.find(element) != col_map.end()) {
        // non_zeroes.push_back(row_idx);
        // non_zeroes.push_back(col_map[element]);
        // }
        // break;
        // }
        // }
        // }
    }

    return non_zeroes;
}

/**
 * @brief Parallel symbolic sparse matrix multiplication.
 *
 * @param[in] A_opt Optional CRS matrix. Only the root process needs to provide this.
 * @param[in] B_opt Optional CCS matrix. Only the root process needs to provide this.
 * @param[in] col_send_strategy The column sending strategy.
 * @param[in] requested_col_strategy The requesting column strategy for optimized all to all.
 * @param[in] communicator MPI communicator. All processes need to provide this.
 * @param[in] final_gather option to gather the results in the end, if false nullprt is returned
 * @param[in] finegrained option for finegrained timing, if false start to end time without final
 * gather is returned
 * @return Optional COO matrix. Only the root process returns this.
 */
std::optional<COOMatrix> parallel_symbolic_spgemm(const std::optional<CRSMatrix> &A_opt,
                                                  const std::optional<CCSMatrix> &B_opt,
                                                  ColSendStrategy col_send_strategy,
                                                  RequestedColStrategy requested_col_strategy,
                                                  MPI_Comm communicator, bool final_gather,
                                                  bool finegrained) {
    // total time without gathering at the end, without function call
    Timer total_timer_without_gather;
    total_timer_without_gather.start();

    int num_workers, rank;
    MPI_Comm_size(communicator, &num_workers);
    MPI_Comm_rank(communicator, &rank);

    CRSMatrix A = rank == 0 ? A_opt.value() : CRSMatrix{};
    CCSMatrix B = rank == 0 ? B_opt.value() : CCSMatrix{};

    // Broadcast number of rows in A
    MPI_Bcast(&A.num_rows, 1, MPI_INT, 0, communicator);

    // Broadcast number of columns in B
    MPI_Bcast(&B.num_cols, 1, MPI_INT, 0, communicator);

    // Rows and cols per rank (will be calculated by rank 0)
    int rows_per_rank = 0;
    int cols_per_rank = 0;

    // Rows and columns to be sent
    std::vector<RowColWrapper> send_rows;
    std::vector<RowColWrapper> send_cols;

    // Vector to collecet the local non-zeroes. Every 2 elements represent the row and column index
    // of a non-zero element
    std::vector<int> non_zeroes;

    // Non empty rows and cols of A and B respectively
    int non_empty_rows = 0;
    int non_empty_cols = 0;

    // Random column assignment to send to all processors
    std::vector<int> col_assignment;

    if (rank == 0) {
        Timer setup_timer;
        // times rank 0 setup time,
        if (finegrained) {
            setup_timer.start();
        }

        // Count non empty rows in A
        for (int row = 0; row < A.num_rows; ++row) {
            int row_start = A.row_ptr[row];
            int row_end = A.row_ptr[row + 1];

            // Check if the row is not empty
            if (row_start < row_end) {
                non_empty_rows++;
            }
        }

        //std::cout << "non empty rows: " << non_empty_rows << std::endl;

        // Count non empty columns in B
        for (int col = 0; col < B.num_cols; ++col) {
            int col_start = B.col_ptr[col];
            int col_end = B.col_ptr[col + 1];

            // Check if the column is not empty
            if (col_start < col_end) {
                non_empty_cols++;
            }
        }

        std::cout << "non empty cols: " << non_empty_cols << std::endl;

        assert(non_empty_rows >= num_workers && "Number of non empty rows must be >= num_workers");
        assert(non_empty_cols >= num_workers && "Number of non empty cols must be >= num_workers");

        // Reserve send columns and rows
        send_rows.reserve(non_empty_rows);
        send_cols.reserve(non_empty_cols);

        // Reserve col assignment
        col_assignment.reserve(non_empty_cols);

        // Prepare the rows of A for sending
        for (int row_idx = 0; row_idx < A.num_rows; row_idx++) {
            int begin = A.row_ptr[row_idx];
            int end = A.row_ptr[row_idx + 1];

            if (end <= begin) {
                continue;
            }

            send_rows.push_back(RowColWrapper(
                row_idx, std::vector<int>(std::make_move_iterator(A.columns.begin() + begin),
                                          std::make_move_iterator(A.columns.begin() + end))));
        }

        // Prepare the columns of B for sending
        for (int col_idx = 0; col_idx < B.num_cols; col_idx++) {
            int begin = B.col_ptr[col_idx];
            int end = B.col_ptr[col_idx + 1];

            if (end <= begin) {
                continue;
            }

            send_cols.push_back(RowColWrapper(
                col_idx, std::vector<int>(std::make_move_iterator(B.rows.begin() + begin),
                                          std::make_move_iterator(B.rows.begin() + end))));
        }

        // Shuffle rows and cols for random assignment
        std::shuffle(send_rows.begin(), send_rows.end(), std::random_device());
        std::shuffle(send_cols.begin(), send_cols.end(), std::random_device());

        for (const auto &col : send_cols) {
            col_assignment.push_back(col.idx);
        }

        if (finegrained) {
            setup_timer.stop();
            std::cout << "Rank 0 setup time: " << setup_timer.duration() << std::endl;
        }
    }

    // Broadcast number of non empty rows and cols
    MPI_Bcast(&non_empty_rows, 1, MPI_INT, 0, communicator);
    MPI_Bcast(&non_empty_cols, 1, MPI_INT, 0, communicator);

    col_assignment.resize(non_empty_cols);

    // Calculate rows and cols per raks based on non empty rows and cols
    rows_per_rank = (int)std::ceil((double)non_empty_rows / ((double)num_workers));
    cols_per_rank = (int)std::ceil((double)non_empty_cols / ((double)num_workers));

    // times the row/col scatter
    Timer row_col_scatter_timer;
    if (finegrained) {
        row_col_scatter_timer.start();
    }

    auto local_rows =
        scatter_row_col(send_rows, num_workers, rows_per_rank, non_empty_rows, rank, communicator);
    // std::cout << "Rank " << rank << " received " << local_rows.size() << " rows" << std::endl;

    auto local_cols =
        scatter_row_col(send_cols, num_workers, cols_per_rank, non_empty_cols, rank, communicator);
    // std::cout << "Rank " << rank << " received " << local_cols.size() << " cols" << std::endl;

    if (finegrained) {
        row_col_scatter_timer.stop();
        std::cout << "Rank " << rank
                  << ": row col scatter time: " << row_col_scatter_timer.duration() << std::endl;
    }

    if (col_send_strategy == ColSendStrategy::BROADCAST) {
        // times broadcast
        Timer broadcast_timer;
        if (finegrained) {
            broadcast_timer.start();
        }

        MPI_Bcast(col_assignment.data(), non_empty_cols, MPI_INT, 0, communicator);

        non_zeroes = compute_non_zeroes_broadcast(local_rows, local_cols, col_assignment,
                                                  cols_per_rank, num_workers, rank, communicator);

        if (finegrained) {
            broadcast_timer.stop();
            std::cout << "Rank " << rank << ": broadcast time: " << broadcast_timer.duration()
                      << std::endl;
        }
    }

    else if (col_send_strategy == ColSendStrategy::ALL_GATHER) {
        // times complete all to all
        Timer ata_timer;
        if (finegrained) {
            ata_timer.start();
        }

        // Send and receive columns from every processor
        auto received_cols =
            send_recv_cols_allgather(local_cols, num_workers, communicator, finegrained, rank);

        // std::cout << "ALLGATHER cols: " << received_cols.size() << std::endl;

        if (finegrained) {
            ata_timer.stop();
            std::cout << "Rank " << rank << ": all to all time: " << ata_timer.duration()
                      << std::endl;
        }

        // times the overlap
        Timer overlap_timer;
        if (finegrained) {
            overlap_timer.start();
        }

        non_zeroes = compute_non_zeros(local_rows, received_cols, false);

        if (finegrained) {
            overlap_timer.stop();
            std::cout << "Rank " << rank
                      << ": overlap time in all to all: " << overlap_timer.duration() << std::endl;
        }
    }

    else if (col_send_strategy == ColSendStrategy::OPTIMIZED_ALL_TO_ALL) {
        Timer opt_ata_timer;
        if (finegrained) {
            opt_ata_timer.start();
        }
        std::vector<std::vector<int>> merged_rows;
        std::vector<std::vector<int>> send_cols;

        if (requested_col_strategy == RequestedColStrategy::MERGED_VECTOR) {
            Timer merged_vec_timer;
            if (finegrained) {
                merged_vec_timer.start();
            }
            merged_rows = compute_and_share_merged_row(
                local_rows, RequestedColStrategy::MERGED_VECTOR, num_workers, communicator);
            send_cols = calc_send_cols(local_cols, merged_rows, num_workers);
            if (finegrained) {
                merged_vec_timer.stop();
                std::cout << "Rank " << rank << ": merged vec time: " << merged_vec_timer.duration()
                          << std::endl;
            }

        } else if (requested_col_strategy == RequestedColStrategy::MERGED_SET) {
            Timer merged_set_timer;
            if (finegrained) {
                merged_set_timer.start();
            }
            merged_rows = compute_and_share_merged_row(local_rows, RequestedColStrategy::MERGED_SET,
                                                       num_workers, communicator);
            send_cols = calc_send_cols(local_cols, merged_rows, num_workers);
            if (finegrained) {
                merged_set_timer.stop();
                std::cout << "Rank " << rank << ": merged set time: " << merged_set_timer.duration()
                          << std::endl;
            }

        } else if (requested_col_strategy == RequestedColStrategy::BLOOM_FILTER) {
            Timer bloom_timer;
            if (finegrained) {
                bloom_timer.start();
            }
            std::vector<std::vector<char>> bloom_filters =
                compute_and_share_bloom_filter(local_rows, num_workers, communicator);
            send_cols = calc_send_cols_bloom(bloom_filters, local_cols, num_workers);
            if (finegrained) {
                bloom_timer.stop();
                std::cout << "Rank " << rank << ": bloom time: " << bloom_timer.duration()
                          << std::endl;
            }
        }

        Timer opt_send_recv_timer;
        if (finegrained) {
            opt_send_recv_timer.start();
        }

        auto received_cols =
            send_recv_cols_alltoall(local_cols, send_cols, num_workers, communicator);
        if (finegrained) {
            opt_send_recv_timer.stop();
            std::cout << "Rank " << rank
                      << ": send and receive time in optimized: " << opt_send_recv_timer.duration()
                      << std::endl;
        }

        if (finegrained) {
            opt_ata_timer.stop();
            std::cout << "Rank " << rank
                      << ": optimized all to all time: " << opt_ata_timer.duration() << std::endl;
        }
        // std::cout << "ALLTOALL cols: " << received_cols.size() << std::endl;

        // times the overlap
        Timer overlap_timer;
        if (finegrained) {
            overlap_timer.start();
        }

        // Calculate overlap
        non_zeroes = compute_non_zeros(local_rows, received_cols, false);

        if (finegrained) {
            overlap_timer.stop();
            std::cout << "Rank " << rank
                      << ": overlap time in optimized all to all: " << overlap_timer.duration()
                      << std::endl;
        }
    }

    total_timer_without_gather.stop();
    std::cout << "Rank " << rank
              << ": total time without gather: " << total_timer_without_gather.duration()
              << std::endl;

    if (!final_gather) {
        return std::nullopt;
    }

    int local_size = non_zeroes.size();
    //std::cout << "Non_zeros count, rank " << rank << " : " << local_size << std::endl;
    std::vector<int> all_sizes(num_workers);

    MPI_Gather(&local_size, 1, MPI_INT, all_sizes.data(), 1, MPI_INT, 0, communicator);

    int global_size = 0;
    MPI_Reduce(&local_size, &global_size, 1, MPI_INT, MPI_SUM, 0, communicator);

    std::vector<int> global_non_zeroes(global_size);

    MPI_Gatherv(non_zeroes.data(), non_zeroes.size(), MPI_INT, global_non_zeroes.data(),
                all_sizes.data(), calc_contiguous_displs(all_sizes).data(), MPI_INT, 0,
                communicator);

    if (rank == 0) {
        COOMatrix res;
        res.reserve(global_size / 2);

        for (size_t i = 0; i + 1 < global_non_zeroes.size(); i += 2) {
            res.push_back(Triplet(global_non_zeroes[i], global_non_zeroes[i + 1], 1));
        }

        // std::cout << "Global_non_zeros count : " << global_size / 2 << std::endl;

        return res;
    } else {
        return std::nullopt;
    }
}
