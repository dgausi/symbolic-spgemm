#pragma once

#include <chrono>

/**
 * @brief A class to measure the time between two points in the code.
 */
class Timer {

    using time_t = std::chrono::high_resolution_clock::time_point;

  public:
    Timer();

    /**
     * @brief Starts the timer.
     * @throw std::runtime_error if the timer is already running.
     */
    void start();

    /**
     * @brief Stops the timer.
     * @throw std::runtime_error if the timer is not running.
     */
    void stop();

    /**
     * @brief Returns the duration of the timer.
     * @throw std::runtime_error if the timer is still running.
     *
     * @return The duration of the timer.
     */
    double duration();

  private:
    bool isrun_;

    time_t tstart_;
    time_t tend_;
};
