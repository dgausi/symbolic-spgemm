#pragma once

#include "common.h"
#include <mpi.h>
#include <optional>

/**
 * BROADCAST: Iterate through all columns and the column owner broadcasts the columns
 * ALL_TO_ALL: (TODO: Rename to ALLGATHER): All processors share all their local columns via
 * ALL_GATHER_v OPTIMIZED_ALL_TO_ALL: All processors share only the columns required by other
 * processors via ALL_TO_ALL_V
 */
enum class ColSendStrategy { BROADCAST, ALL_GATHER, OPTIMIZED_ALL_TO_ALL };

/**
 * Specify configuration of the OPTIMIZED_ALL_TO_ALL:
 * MERGED_VECTOR: Combine the processor's local rows by inserting to a vector and sorting (contains
 * duplicates) MERGED_SET: Combine the processor's local rows by inserting to a set (auto-sorted),
 * thus reducing comms overhead. BLOOM_FILTER: Use a fixed-size vector and hashes (bloom filter) to
 * probabilistically determine which columns are required
 */
enum class RequestedColStrategy { MERGED_VECTOR, MERGED_SET, BLOOM_FILTER };

inline std::ostream &operator<<(std::ostream &os, ColSendStrategy col_send_strategy) {
    switch (col_send_strategy) {
    case ColSendStrategy::ALL_GATHER:
        return os << "ALL_GATHER";
    case ColSendStrategy::OPTIMIZED_ALL_TO_ALL:
        return os << "OPTIMIZED_ALL_TO_ALL";
    case ColSendStrategy::BROADCAST:
        return os << "BROADCAST";
    };

    return os << "UNDEFINED";
}

inline std::ostream &operator<<(std::ostream &os, RequestedColStrategy check_overlap_strategy) {
    switch (check_overlap_strategy) {
    case RequestedColStrategy::MERGED_VECTOR:
        return os << "MERGED_VECTOR";
    case RequestedColStrategy::MERGED_SET:
        return os << "MERGED_SET";
    case RequestedColStrategy::BLOOM_FILTER:
        return os << "BLOOM_FILTER";
    };

    return os << "UNDEFINED";
}

/**
 * @brief Parallel symbolic sparse matrix multiplication.
 *
 * @param[in] A_opt Optional CRS matrix. Only the root process needs to provide this.
 * @param[in] B_opt Optional CCS matrix. Only the root process needs to provide this.
 * @param[in] col_send_strategy The column sending strategy.
 * @param[in] requested_col_strategy The requesting column strategy for optimized all to all.
 * @param[in] communicator MPI communicator. All processes need to provide this.
 * @param[in] final_gather option to gather the results in the end, if false nullprt is returned
 * @param[in] finegrained option for finegrained timing, if false start to end time without final
 * gather is returned
 * @return Optional COO matrix. Only the root process returns this.
 */
std::optional<COOMatrix> parallel_symbolic_spgemm(const std::optional<CRSMatrix> &A_opt,
                                                  const std::optional<CCSMatrix> &B_opt,
                                                  ColSendStrategy col_send_strategy,
                                                  RequestedColStrategy requested_col_strategy,
                                                  MPI_Comm communicator, bool final_gather,
                                                  bool finegrained);
