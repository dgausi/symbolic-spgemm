#include "timer.h"
#include <chrono>
#include <stdexcept>

Timer::Timer() { isrun_ = false; }

void Timer::start() {
    if (isrun_) {
        throw std::runtime_error("The timer is already running!");
    }

    tstart_ = std::chrono::high_resolution_clock::now();
    isrun_ = true;
}

void Timer::stop() {
    if (!isrun_) {
        throw std::runtime_error("The timer is not running!");
    }

    tend_ = std::chrono::high_resolution_clock::now();
    isrun_ = false;
}

double Timer::duration() {

    if (isrun_) {
        throw std::runtime_error("The timer is still running!");
    }

    return std::chrono::duration<double>(tend_ - tstart_).count();
}
