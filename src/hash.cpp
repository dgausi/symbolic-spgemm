#include "hash.h"
#include <cstddef>

size_t hashFunction1(int key, size_t size) { return static_cast<size_t>(key) % size; }

size_t hashFunction2(int key, size_t size) {
    // A simple bitwise hash function
    return (key ^ (key >> 16)) % size;
}

size_t hashFunction3(int key, size_t size) {
    // Jenkins one-at-a-time hash function
    size_t hash = 0;
    while (key) {
        hash += key & 0xFF;
        hash += (hash << 10);
        hash ^= (hash >> 6);
        key >>= 8;
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash % size;
}

size_t hashFunction4(int key, size_t size) {
    // Bob Jenkins' Mix Function
    key = (key ^ 0xdeadbeef) + (key << 4);
    key = key ^ (key >> 10);
    key = key + (key << 7);
    key = key ^ (key >> 13);
    return static_cast<size_t>(key) % size;
}

size_t hashFunction5(int key, size_t size) {
    // Pearson hashing
    static const unsigned char table[256] = {
        // shuffled table of all 8-bit numbers (0-255)
    };
    size_t hash = 0;
    for (size_t i = 0; i < sizeof(int); ++i) {
        hash = table[(hash ^ ((key >> (i * 8)) & 0xFF)) & 0xFF];
    }
    return hash % size;
}

size_t hashFunction6(int key, size_t size) {
    // Thomas Wang's Integer Hash Function
    key = ~key + (key << 15);
    key = key ^ (key >> 12);
    key = key + (key << 2);
    key = key ^ (key >> 4);
    key = key * 2057;
    key = key ^ (key >> 16);
    return static_cast<size_t>(key) % size;
}

size_t hashFunction7(int key, size_t size) {
    // ELF Hash Function
    size_t hash = 0, x = 0;
    for (size_t i = 0; i < sizeof(int); ++i) {
        hash = (hash << 4) + ((key >> (i * 8)) & 0xFF);
        if ((x = hash & 0xF0000000L) != 0) {
            hash ^= (x >> 24);
        }
        hash &= ~x;
    }
    return hash % size;
}

size_t hashFunction8(int key, size_t size) {
    // FNV-1a Hash Function
    size_t hash = 2166136261U;
    for (size_t i = 0; i < sizeof(int); ++i) {
        hash ^= (key >> (i * 8)) & 0xFF;
        hash *= 16777619;
    }
    return hash % size;
}

size_t hashFunction9(int key, size_t size) {
    // MurmurHash2
    const unsigned int m = 0x5bd1e995;
    unsigned int hash = 0;
    key ^= key >> 16;
    key *= m;
    key ^= key >> 13;
    key *= m;
    key ^= key >> 16;
    hash += key;
    hash *= m;
    hash ^= hash >> 16;
    return hash % size;
}

size_t hashFunction10(int key, size_t size) {
    // DJB2 Hash Function
    size_t hash = 5381;
    for (size_t i = 0; i < sizeof(int); ++i) {
        hash = (hash << 5) + ((key >> (i * 8)) & 0xFF);
    }
    return hash % size;
}

size_t hashFunction11(int key, size_t size) {
    // Jenkins' Lookup3 Hash Function
    size_t a = 0xdeadbeef;
    size_t b = 0xdeadbeef;
    size_t c = 0xdeadbeef;
    a += key;
    b += key << 5;
    c += key << 9;
    a ^= c;
    a -= (c << 14) | (c >> (32 - 14));
    b ^= a;
    b -= (a << 11) | (a >> (32 - 11));
    c ^= b;
    c -= (b << 25) | (b >> (32 - 25));
    a ^= c;
    a -= (c << 16) | (c >> (32 - 16));
    b ^= a;
    b -= (a << 4) | (a >> (32 - 4));
    c ^= b;
    c -= (b << 14) | (b >> (32 - 14));
    a ^= c;
    a -= (c << 24) | (c >> (32 - 24));
    return a % size;
}
