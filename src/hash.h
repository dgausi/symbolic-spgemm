#pragma once
#include <cstddef>

size_t hashFunction1(int key, size_t size);

size_t hashFunction2(int key, size_t size);

size_t hashFunction3(int key, size_t size);

size_t hashFunction4(int key, size_t size);

size_t hashFunction5(int key, size_t size);

size_t hashFunction6(int key, size_t size);

size_t hashFunction7(int key, size_t size);

size_t hashFunction8(int key, size_t size);

size_t hashFunction9(int key, size_t size);

size_t hashFunction10(int key, size_t size);

size_t hashFunction11(int key, size_t size);
