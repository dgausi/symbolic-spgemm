#pragma once

#include "common.h"

COOMatrix sequential_spgemm(const CRSMatrix &A, const CCSMatrix &B);
