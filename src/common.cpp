#include "common.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <unordered_set>

// Function to read a matrix from a Matrix Market file and return it in CRS format
CRSMatrix readMatrixFromMatrixMarketCRS(const std::string &filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        throw std::runtime_error("Failed to open the file.");
    }

    std::string line;
    int num_rows, num_cols, num_entries;

    // Skip comments and read the second line to get matrix dimensions and number of entries
    do {
        if (!std::getline(file, line)) {
            throw std::runtime_error("Failed to find the second line.");
        }
    } while (line.at(0) == '%');

    if (sscanf(line.c_str(), "%d %d %d", &num_rows, &num_cols, &num_entries) != 3) {
        throw std::runtime_error("Failed to read matrix dimensions and number of entries.");
    }

    std::vector<COOEntry> coo_entries;
    coo_entries.reserve(num_entries);

    while (std::getline(file, line)) {
        if (line.at(0) == '%') {
            // Skip comments
            continue;
        } else {
            int row, col;
            double value;
            if (sscanf(line.c_str(), "%d %d %lf", &row, &col, &value) == 3) {
                // Adjust for 1-based indexing in the file by subtracting 1
                coo_entries.push_back({row - 1, col - 1, value});
            }
        }
    }

    file.close();

    // Sort COO entries based on row index
    std::sort(coo_entries.begin(), coo_entries.end(), [](const COOEntry &a, const COOEntry &b) {
        return a.row < b.row || (a.row == b.row && a.col < b.col);
    });

    // Initialize CRSMatrix
    CRSMatrix crs_matrix;
    crs_matrix.num_rows = num_rows;
    crs_matrix.num_cols = num_cols;
    crs_matrix.num_non_zeros = num_entries;
    crs_matrix.row_ptr.resize(num_rows + 1, 0);

    // Populate CRS arrays
    crs_matrix.values.reserve(num_entries);
    crs_matrix.columns.reserve(num_entries);

    int current_row = 1; // Start with the first row
    for (int i = 0; i < num_entries; i++) {
        const COOEntry &entry = coo_entries[i];
        int row = entry.row;
        int col = entry.col;
        double value = entry.value;

        crs_matrix.values.push_back(value);
        crs_matrix.columns.push_back(col);

        while (current_row <= row) {
            crs_matrix.row_ptr[current_row] = i;
            current_row++;
        }
    }
    crs_matrix.row_ptr.back() = num_entries;

    return crs_matrix;
}

// Function to read a matrix from a Matrix Market file and return it in CCS format
CCSMatrix readMatrixFromMatrixMarketCCS(const std::string &filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        throw std::runtime_error("Failed to open the file.");
    }

    std::string line;
    int num_rows, num_cols, num_entries;

    // Skip comments and read the second line to get matrix dimensions and number of entries
    do {
        if (!std::getline(file, line)) {
            throw std::runtime_error("Failed to find the second line.");
        }
    } while (line.at(0) == '%');

    if (sscanf(line.c_str(), "%d %d %d", &num_rows, &num_cols, &num_entries) != 3) {
        throw std::runtime_error("Failed to read matrix dimensions and number of entries.");
    }

    std::vector<COOEntry> coo_entries;
    coo_entries.reserve(num_entries);

    while (std::getline(file, line)) {
        if (line.at(0) == '%') {
            // Skip comments
            continue;
        } else {
            int row, col;
            double value;
            if (sscanf(line.c_str(), "%d %d %lf", &row, &col, &value) == 3) {
                // Adjust for 1-based indexing in the file by subtracting 1
                coo_entries.push_back({row - 1, col - 1, value});
            }
        }
    }

    file.close();

    // Sort COO entries based on column index
    std::sort(coo_entries.begin(), coo_entries.end(), [](const COOEntry &a, const COOEntry &b) {
        return a.col < b.col || (a.col == b.col && a.row < b.row);
    });

    // Initialize CCSMatrix
    CCSMatrix ccs_matrix;
    ccs_matrix.num_rows = num_rows;
    ccs_matrix.num_cols = num_cols;
    ccs_matrix.num_non_zeros = num_entries;
    ccs_matrix.col_ptr.resize(num_cols + 1, 0);

    // Populate CCS arrays
    ccs_matrix.values.reserve(num_entries);
    ccs_matrix.rows.reserve(num_entries);

    int current_col = 1; // Start with the first column
    for (int i = 0; i < num_entries; i++) {
        const COOEntry &entry = coo_entries[i];
        int row = entry.row;
        int col = entry.col;
        double value = entry.value;

        ccs_matrix.values.push_back(value);
        ccs_matrix.rows.push_back(row);

        while (current_col <= col) {
            ccs_matrix.col_ptr[current_col] = i;
            current_col++;
        }
    }
    ccs_matrix.col_ptr.back() = num_entries;

    return ccs_matrix;
}

void printAsMatrix(const COOMatrix &matrix, int numRows, int numCols) {
    // Create a dense matrix initialized with zeros
    std::vector<std::vector<double>> denseMatrix(numRows, std::vector<double>(numCols, 0.0));

    // Fill the dense matrix with values from the COO matrix
    for (const Triplet &triplet : matrix) {
        denseMatrix[triplet.i][triplet.j] = triplet.val;
    }

    // Print the dense matrix
    for (int i = 0; i < numRows; ++i) {
        for (int j = 0; j < numCols; ++j) {
            std::cout << denseMatrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void printCRSMatrix(const CRSMatrix &crsMatrix) {
    std::cout << "CRS Format Matrix:" << std::endl;
    std::cout << "Number of Rows: " << crsMatrix.num_rows << std::endl;
    std::cout << "Number of Columns: " << crsMatrix.num_cols << std::endl;
    std::cout << "Number of Non-Zero Elements: " << crsMatrix.num_non_zeros << std::endl;

    std::cout << "Values: ";
    for (int i = 0; i < crsMatrix.num_non_zeros; ++i) {
        std::cout << crsMatrix.values[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Columns: ";
    for (int i = 0; i < crsMatrix.num_non_zeros; ++i) {
        std::cout << crsMatrix.columns[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Row Pointers: ";
    for (int i = 0; i <= crsMatrix.num_rows; ++i) {
        std::cout << crsMatrix.row_ptr[i] << " ";
    }
    std::cout << std::endl;
}

// Print CCS Format Matrix
void printCCSMatrix(const CCSMatrix &ccsMatrix) {
    std::cout << "CCS Format Matrix:" << std::endl;
    std::cout << "Number of Rows: " << ccsMatrix.num_rows << std::endl;
    std::cout << "Number of Columns: " << ccsMatrix.num_cols << std::endl;
    std::cout << "Number of Non-Zero Elements: " << ccsMatrix.num_non_zeros << std::endl;

    std::cout << "Values: ";
    for (int i = 0; i < ccsMatrix.num_non_zeros; ++i) {
        std::cout << ccsMatrix.values[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Rows: ";
    for (int i = 0; i < ccsMatrix.num_non_zeros; ++i) {
        std::cout << ccsMatrix.rows[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Column Pointers: ";
    for (int i = 0; i <= ccsMatrix.num_cols; ++i) {
        std::cout << ccsMatrix.col_ptr[i] << " ";
    }
    std::cout << std::endl;
}

// Adapted from https://stackoverflow.com/a/67963037
COOMatrix generate_random_matrix(size_t rows, size_t cols, double p) {
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> rowdis(0, rows - 1);
    std::uniform_int_distribution<> coldis(0, cols - 1);

    COOMatrix coo_matrix;
    size_t nnz = (size_t)(rows * (cols * p));
    std::unordered_set<size_t> nnz_pos;
    for (size_t i = 0; i < nnz; ++i) {
        auto r = rowdis(gen);
        auto c = coldis(gen);
        size_t pos = r * cols + c;
        while (nnz_pos.find(pos) != nnz_pos.end()) {
            r = rowdis(gen);
            c = coldis(gen);
            pos = r * cols + c;
        }

        nnz_pos.insert(pos);
        coo_matrix.push_back(Triplet(r, c, 1));
    }

    return coo_matrix;
}

CCSMatrix coo_to_ccs(COOMatrix &matrix, size_t rows, size_t cols) {
    auto nnz = matrix.size();

    // Sort COO entries based on column index
    std::sort(matrix.begin(), matrix.end(), [](const Triplet &a, const Triplet &b) {
        return a.j < b.j || (a.j == b.j && a.i < b.i);
    });

    // Initialize CCSMatrix
    CCSMatrix ccs_matrix;
    ccs_matrix.num_rows = rows;
    ccs_matrix.num_cols = cols;
    ccs_matrix.num_non_zeros = nnz;
    ccs_matrix.col_ptr.resize(cols + 1, 0);

    // Populate CCS arrays
    ccs_matrix.values.reserve(nnz);
    ccs_matrix.rows.reserve(nnz);

    int current_col = 1; // Start with the first column
    for (size_t i = 0; i < nnz; i++) {
        const Triplet &entry = matrix[i];
        int row = entry.i;
        int col = entry.j;
        double value = entry.val;

        ccs_matrix.values.push_back(value);
        ccs_matrix.rows.push_back(row);

        while (current_col <= col) {
            ccs_matrix.col_ptr[current_col] = i;
            current_col++;
        }

        ccs_matrix.col_ptr[current_col] = i + 1;
    }

    while ((size_t)current_col + 1 < ccs_matrix.col_ptr.size() - 1) {
        ccs_matrix.col_ptr[current_col + 1] = ccs_matrix.col_ptr[current_col];
        current_col++;
    }

    ccs_matrix.col_ptr.back() = nnz;

    return ccs_matrix;
}

CRSMatrix coo_to_crs(COOMatrix &matrix, size_t rows, size_t cols) {
    auto nnz = matrix.size();

    // Sort COO entries based on row index
    std::sort(matrix.begin(), matrix.end(), [](const Triplet &a, const Triplet &b) {
        return a.i < b.i || (a.i == b.i && a.j < b.j);
    });

    // Initialize CRSMatrix
    CRSMatrix crs_matrix;
    crs_matrix.num_rows = rows;
    crs_matrix.num_cols = cols;
    crs_matrix.num_non_zeros = nnz;
    crs_matrix.row_ptr.resize(rows + 1, 0);

    // Populate CRS arrays
    crs_matrix.values.reserve(nnz);
    crs_matrix.columns.reserve(nnz);

    int current_row = 1; // Start with the first row
    for (size_t i = 0; i < nnz; i++) {
        const Triplet &entry = matrix[i];
        int row = entry.i;
        int col = entry.j;
        double value = entry.val;

        crs_matrix.values.push_back(value);
        crs_matrix.columns.push_back(col);

        while (current_row <= row) {
            crs_matrix.row_ptr[current_row] = i;
            current_row++;
        }

        crs_matrix.row_ptr[current_row] = i + 1;
    }

    while ((size_t)current_row + 1 < crs_matrix.row_ptr.size() - 1) {
        crs_matrix.row_ptr[current_row + 1] = crs_matrix.row_ptr[current_row];
        current_row++;
    }

    crs_matrix.row_ptr.back() = nnz;

    return crs_matrix;
}

CCSMatrix generate_random_matrix_ccs(size_t rows, size_t cols, double p) {
    auto coo_matrix = generate_random_matrix(rows, cols, p);
    return coo_to_ccs(coo_matrix, rows, cols);
}

CRSMatrix generate_random_matrix_crs(size_t rows, size_t cols, double p) {
    auto coo_matrix = generate_random_matrix(rows, cols, p);
    return coo_to_crs(coo_matrix, rows, cols);
}
