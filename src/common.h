#pragma once

#include <string>
#include <vector>

struct COOEntry {
    int row, col;
    double value;
};

struct CRSMatrix {
    std::vector<double> values; // Non-zero values of the matrix
    std::vector<int> columns;   // Column indices of the non-zero values
    std::vector<int> row_ptr;   // Row pointers
    int num_rows;
    int num_cols;
    int num_non_zeros;

    // The rest of your CRSMatrix code can remain the same.
};

struct CCSMatrix {
    std::vector<double> values; // Non-zero values of the matrix
    std::vector<int> rows;      // Row indices of the non-zero values
    std::vector<int> col_ptr;   // Column pointers
    int num_rows;
    int num_cols;
    int num_non_zeros;
    // The rest of your CCSMatrix code can remain the same.
};

struct Triplet {
    int i;
    int j;
    double val;

    Triplet(int i_, int j_, double val_) : i(i_), j(j_), val(val_) {}
};

using COOMatrix = std::vector<Triplet>;

CRSMatrix readMatrixFromMatrixMarketCRS(const std::string &filename);

CCSMatrix readMatrixFromMatrixMarketCCS(const std::string &filename);

void printAsMatrix(const COOMatrix &matrix, int numRows, int numCols);

void printCRSMatrix(const CRSMatrix &crsMatrix);

void printCCSMatrix(const CCSMatrix &ccsMatrix);

CCSMatrix coo_to_ccs(COOMatrix &matrix, size_t rows, size_t cols);

CRSMatrix coo_to_crs(COOMatrix &matrix, size_t rows, size_t cols);

COOMatrix generate_random_matrix(size_t rows, size_t cols, double p);

CCSMatrix generate_random_matrix_ccs(size_t rows, size_t cols, double p);

CRSMatrix generate_random_matrix_crs(size_t rows, size_t cols, double p);
