# Symbolic SpGEMM

# How to

## Setup

1. Clone the repo.
2. Install [meson](https://mesonbuild.com/Getting-meson.html).
3. Run `meson setup build` from the project root.

## Build

1. `cd build`
2. `meson compile`

## Run the test suite

1. `cd build`
2. `meson test`

## benchmark
1. `cd build/test`
2. run the file you want ex: mpirun ./mpi-benchmark 100 100 alltoall bloom_filter false true
3. mpirun -n x rest_of_command runs the programm on x cores


