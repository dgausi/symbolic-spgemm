#include "timer.hpp"
#include <chrono>
#include <stdexcept>

Time::Time(){
    isrun_=false;
}

void Time::start(){
    if(isrun_){
        throw std::runtime_error("clock already running!!");
    }

tstart_=std::chrono::high_resolution_clock::now();
isrun_=true;
}

void Time::stop(){
    if(!isrun_){
        throw std::runtime_error("the clock dindt start!");
    }

    tend_=std::chrono::high_resolution_clock::now();
    isrun_=false;
}

double Time::duration(){

    if(isrun_){
        throw std::runtime_error("clock already running!!");
    }

    return std::chrono::duration<double>(tend_ - tstart_).count();
}