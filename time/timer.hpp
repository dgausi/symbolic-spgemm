#ifndef TIMER.HPP
#define TIMER.HPP

#include <chrono>

class Time{

    using time_t=std::chrono::high_resolution_clock::time_point;

    public:

    Time();

    //the current state has to be not running already
    //begins the timer to run by denoting the time to tstart, changes state to isrunning
    void start();


    //the object has to be in a runningstate
    //stops the running by denoting the time to tend, changes state to is not running
    void stop();


    //state has to be in not running
    //returns the difference in seconds between tend and tstart
    double duration();




    private:
    bool isrun_;

    time_t tstart_;
    time_t tend_;

};




#endif