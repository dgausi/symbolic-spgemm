import matplotlib.pyplot as plt
import numpy as np

# Read data from the benchmark_results.txt file
data = np.loadtxt('cmake-build-debug/benchmark_results.txt')

# Extract columns
N_values = data[:, 0]
avx_durations = data[:, 1]
bs_durations = data[:, 2]
std_bs_durations = data[:, 3]
point_durations = data[:, 4]

# Define a color palette
color_avx = 'tab:blue'
color_binary= 'tab:orange'
color_moving_ptr= 'tab:green'
color_hash_table= 'tab:brown'



# Plotting
fig, ax = plt.subplots(figsize=(8, 6))  # Adjust the figure size

# Plot only the first two values for AVX
ax.plot(N_values[:2], avx_durations[:2], label='AVX', marker='o', linestyle='-', color=color_avx)

#ax.plot(N_values, avx_durations, label='AVX', marker='o', linestyle='-', color=color_avx)
ax.plot(N_values, bs_durations, label='Binary Search', marker='s', linestyle='--', color=color_binary)
ax.plot(N_values, point_durations, label='Moving Pointers', marker='^', linestyle='-', color=color_moving_ptr)
ax.plot(N_values, std_bs_durations, label='Hash_map', marker='^', linestyle='-', color=color_hash_table)

# Set log scale on x-axis
#ax.set_xscale('log', base=10)
ax.set_yscale('log', base=10)
# Add labels and title
ax.set_xlabel('Number of rows/cols $N$', fontsize=14)
ax.set_ylabel('Duration (s)', fontsize=14)
#ax.set_title('Performance of Has-Overlap', fontsize=16)

# Set ticks font size
ax.tick_params(axis='both', which='major', labelsize=12)

# Add grid lines
ax.grid(True, linestyle='--', alpha=0.7)

# Add legend with a frame
ax.legend(fontsize=12, loc='upper left', frameon=True)
# Add main title
#plt.suptitle('Main Title: Performance of Different Methods', fontsize=12, y=1.02)
ax.set_title('Performance of Different Methods for Has-Overlap', fontsize=12)


# Save the figure
plt.savefig('performance_plot_with_hash_table.png', dpi=600, bbox_inches='tight')

# Display the plot
plt.show()
