//
// Created by ramu on 03.12.23.
//
#include <iostream>
#include <fstream>
#include <immintrin.h>
#include <vector>
#include <algorithm>
#include <random>
#include <set>
#include <iomanip>
#include <cmath>
#include <cassert>
#include <unordered_map>
#include "time/timer.hpp"

struct RowColWrapper {
    RowColWrapper() = default;

    /**
     * @brief Construct a new RowColWrapper from a CRS row or a CCS column.
     *
     * @param[in] idx The index of the row or column.
     * @param[in] size The number of elements in the row or column.
     * @param[in] indices The indices of the elements in the row or column.
     */
    RowColWrapper(int idx, std::vector<int> indices) : indices(std::move(indices)), idx(idx) {}

    static std::unordered_map<int, RowColWrapper> parse_recvbuffer(std::vector<int> recvbuffer) {
        std::unordered_map<int, RowColWrapper> res;

        for (size_t i = 0; i < recvbuffer.size(); i += recvbuffer[i] + 2) {
            auto &size = recvbuffer[i];
            auto &idx = recvbuffer[i + 1];

            assert(size > 0);

            RowColWrapper row_col(
                idx, std::vector<int>(std::make_move_iterator(recvbuffer.begin() + i + 2),
                                      std::make_move_iterator(recvbuffer.begin() + i + 2 + size)));
            res[row_col.idx] = row_col;
        }

        return res;
    }

    int size() const { return indices.size(); }

    int total_size() const { return size() + 2; }

    std::vector<int> indices;
    int idx;
};


// Function to save a vector to a file
template <typename T>
void saveVectorToFile(const std::vector<T>& vec, const std::string& filename) {
    std::ofstream file(filename);
    if (file.is_open()) {
        for (const auto& value : vec) {
            file << value << " ";
        }
        file.close();
    } else {
        std::cerr << "Error: Unable to open the file for writing." << std::endl;
    }
}

// Function to read a vector from a file
template <typename T>
std::vector<T> readVectorFromFile(const std::string& filename) {
    std::ifstream file(filename);
    std::vector<T> vec;
    if (file.is_open()) {
        T value;
        while (file >> value) {
            vec.push_back(value);
        }
        file.close();
    } else {
        std::cerr << "Error: Unable to open the file for reading." << std::endl;
    }
    return vec;
}


//just to print
template<class t> inline void log(const __m256i & value)
{
    const size_t n = sizeof(__m256i) / sizeof(t);
    t buffer[n];
    _mm256_storeu_si256((__m256i*)buffer, value);
    for (int i = 0; i < n; i++)
        std::cout << buffer[i] << " ";
}



std::vector<int> generateRandomSortedVector(int N, int vectorSize) {
    std::vector<int> values(N);
    std::iota(values.begin(), values.end(), 0);  // Fill with consecutive values [0, 1, 2, ..., N-1]

    // Shuffle the vector
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(values.begin(), values.end(), g);

    // Take the first 'vectorSize' elements
    values.resize(vectorSize);

    // Sort the vector
    std::sort(values.begin(), values.end());

    return values;
}

bool has_overlap_moving_ptr(const std::vector<int> &row, const std::vector<int> &col){

    int i=0;
    int j=0;
    do {
        if(row[i]==col[j]){
            return true;
        }
        else if(row[i]<col[j]){
            ++i;
        }
        else{
            ++j;
        }
    } while (i<row.size()&&j<col.size());
    return false;
}

bool has_overlap_Std_BS(const std::vector<int> &row, const std::vector<int> &col) {

    for (const auto &row_idx : row) {
        if (std::binary_search(col.begin(), col.end(), row_idx)) {
            return true;
        }
    }
    return false;
}
bool binary_search(const std::vector<int>& arr, int target) {
    int left = 0;
    int right = arr.size() - 1;

    while (left <= right) {
        int mid = left + (right - left) / 2;

        if (arr[mid] == target) {
            return true;
        } else if (arr[mid] < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }

    return false;
}

bool has_overlap_binary_search(const std::vector<int>& a, const std::vector<int>& b) {


    // Use the smaller vector for binary search to optimize

    const std::vector<int>& smaller = (a.size() <= b.size()) ? a : b;
    const std::vector<int>& larger = (a.size() < b.size()) ? a : b;

    // Perform binary search for each element in the smaller vector
    for (int num : smaller) {
        if (binary_search(larger, num)) {
            return true;
        }
    }

    return false;
}


inline bool has_overlap(const std::vector<int> &row, const std::vector<int> &col, int N, Time &tav) {


    //the size of the whole binary array to be checked
    int size=N;

    //size of uint 32 bit
    //size of avx 256
    // this means we can do 8 uints bitwise and at same time
    //size of the array which stores the uints for the bitwise and it must be a multiple of 8
    size = (size+31)/32;

    if(size % 8 !=0){
        size += 8 - (size%8);
    }

    alignas(32) std::vector<unsigned int> Au(size, 0);
    alignas(32) std::vector<unsigned int> Bu(size, 0);

    //fill A and B at first

    for (int i = 0; i < row.size()-(row.size()%8); i += 8) {
        int idx1 = row[i] / 32;    Au[idx1] |= (1U << (31 - (row[i] % 32)));
        int idx2 = row[i+1] / 32;  Au[idx2] |= (1U << (31 - (row[i+1] % 32)));
        int idx3 = row[i+2] / 32;  Au[idx3] |= (1U << (31 - (row[i+2] % 32)));
        int idx4 = row[i+3] / 32;  Au[idx4] |= (1U << (31 - (row[i+3] % 32)));
        int idx5 = row[i+4] / 32;  Au[idx5] |= (1U << (31 - (row[i+4] % 32)));
        int idx6 = row[i+5] / 32;  Au[idx6] |= (1U << (31 - (row[i+5] % 32)));
        int idx7 = row[i+6] / 32;  Au[idx7] |= (1U << (31 - (row[i+6] % 32)));
        int idx8 = row[i+7] / 32;  Au[idx8] |= (1U << (31 - (row[i+7] % 32)));
    }
    // Process the remaining elements
    for (int i = row.size() - (row.size() % 8); i < row.size(); ++i) {
        int idx = row[i] / 32;
        Au[idx] |= (1U << (31 - (row[i] % 32)));
    }

    for (int i = 0; i < col.size()-(col.size()%8); i += 8) {
        int idx1 = col[i] / 32;    Bu[idx1] |= (1U << (31 - (col[i] % 32)));
        int idx2 = col[i+1] / 32;  Bu[idx2] |= (1U << (31 - (col[i+1] % 32)));
        int idx3 = col[i+2] / 32;  Bu[idx3] |= (1U << (31 - (col[i+2] % 32)));
        int idx4 = col[i+3] / 32;  Bu[idx4] |= (1U << (31 - (col[i+3] % 32)));
        int idx5 = col[i+4] / 32;  Bu[idx5] |= (1U << (31 - (col[i+4] % 32)));
        int idx6 = col[i+5] / 32;  Bu[idx6] |= (1U << (31 - (col[i+5] % 32)));
        int idx7 = col[i+6] / 32;  Bu[idx7] |= (1U << (31 - (col[i+6] % 32)));
        int idx8 = col[i+7] / 32;  Bu[idx8] |= (1U << (31 - (col[i+7] % 32)));
    }
    for (int i = col.size() - (col.size() % 8); i < col.size(); ++i) {
        int idx = col[i] / 32;
        Bu[idx] |= (1U << (31 - (col[i] % 32)));
    }


    //now i must perform AVX on Au & Bu
    // Process 8 elements (32 bits each) at a time using AVX

    //tav.start();
    for (size_t i = 0; i < size; i += 8) {

        // Check if both vectors have all zeros in the current 256-bit block
        if (_mm256_testz_si256(
                _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&Au[i])),
                _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&Bu[i]))
        )) {
            // Both vectors have all zeros in this block, move to the next block
            continue;
        }
        // Load 256 bits (8 elements) from vectors a and b into AVX registers
        __m256i va = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&Au[i]));
        __m256i vb = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&Bu[i]));

        // Perform bitwise AND operation
        __m256i result_vec = _mm256_and_si256(va, vb);


        //log<long>(result_vec);
        // Store the result back to the result vector
        if(!_mm256_testz_si256(result_vec, result_vec)){
            return true;
        }
    }
    return false;
}

int main(){
    // Output file to store benchmark data
    std::ofstream outputFile("benchmark_results.txt");

    if (!outputFile.is_open()) {
        std::cerr << "Error: Unable to open the output file." << std::endl;
        return 1;
    }

    // Vary N values for benchmarking
    //std::vector<int> N_values = { 10000, 100000, 1000000, 10000000, 20000000,40000000,80000000, 100000000};
    int repeats= 10;
    int N =5000;


    for (int f =1;f<=repeats;++f) {

        N +=5000*f*f;
        double tavo;
        double tbso;
        double tstdbso;
        double tpointo;
        int rep=5;
        int overlapp_counter=0;

        for(int k=0;k<rep;++k){
            //definition of sparcity
            int vectorSize = N*0.001;
            if(N>99999){
                vectorSize = vectorSize/10;
            }

            int rowsperpor = N/48;



            
            

            std::unordered_map<int, RowColWrapper> Rows;
            std::unordered_map<int, RowColWrapper> Cols;

            
            std::cout<<vectorSize<<std::endl;
            for(int i=0;i<rowsperpor;++i){

                int col_idx = rand() % N;
                int row_idx = rand()% N;
                std::vector<int> rows = generateRandomSortedVector(N, vectorSize);
                std::vector<int> cols = generateRandomSortedVector(N, vectorSize);

                //random col_inx and random row_idx
                RowColWrapper rci = RowColWrapper(row_idx,rows);
                RowColWrapper cci = RowColWrapper(col_idx,rows);
                Rows[row_idx]=rci;
                Cols[col_idx]=cci;

            }

            // Save vectors to files
            // Read vectors from files
            //std::vector<int>rows = readVectorFromFile<int>("../randomgenerated_vectors/rows_" + std::to_string(N) + ".txt");
            //std::vector<int>cols = readVectorFromFile<int>("../randomgenerated_vectors/cols_" + std::to_string(N) + ".txt");

            Time tav;
            Time tbs;
            Time tstdbs;
            Time tpoint;



            // Benchmark the binary search version
            tbs.start();
            for (const auto &[row_idx, row] : Rows) {
                for (const auto &[col_idx, col] : Cols) {
                    if (has_overlap_binary_search(row.indices, col.indices)) {
                        ++overlapp_counter;
                    }
                }
            }
            tbs.stop();

            std::cout<<"BS Done"<<std::endl;

            // Benchmark Hash_table
            tstdbs.start();
            std::unordered_map<int, std::vector<int>> col_map; // value, col_idx
            for (const auto &[col_idx, col] : Cols) {
                for (auto element : col.indices) {
                    col_map[element].push_back(col_idx); // TODO: Pre-allocate col_map[element]
                }
            }

            for (const auto &[row_idx, row] : Rows) {
                for (auto element : row.indices) {
                    if (col_map.find(element) != col_map.end()) {
                        for (auto col_idx : col_map[element]) {
                            ++overlapp_counter;
                        }
                    }
                }
            }
            tstdbs.stop();

            std::cout<<"Hash Done"<<std::endl;

            tpoint.start();
            for (const auto &[row_idx, row] : Rows) {
                for (const auto &[col_idx, col] : Cols) {
                    if (has_overlap_moving_ptr(row.indices, col.indices)) {
                        ++overlapp_counter;
                    }
                }
            }
            tpoint.stop();

            if(f<=2){
                tav.start();
                for (const auto &[row_idx, row] : Rows) {
                    for (const auto &[col_idx, col] : Cols) {
                        if (has_overlap(row.indices, col.indices,N,tav)) {
                            ++overlapp_counter;
                        }
                    }
                }
                tav.stop();
                tavo+=tav.duration();
                std::cout<<"AVX Done"<<std::endl;
            }
            // Benchmark the AVX version



            tbso+=tbs.duration();
            tstdbso+=tstdbs.duration();
            tpointo+=tpoint.duration();
        }
        // Print results to console
        std::cout << "Matrix dimension N: " << N << std::endl;
        if(f<=2){
            std::cout << "Duration for the AVX: " << tavo/rep << " s" << std::endl;
        }

        std::cout << "Duration for the BS: " << tbso/rep << " s" << std::endl;
        std::cout << "Duration for the std BS: " << tstdbso/rep << " s" << std::endl;
        std::cout << "Duration moving ptr: " << tpointo/rep << " s" << std::endl;
        std::cout << "overlaps: "<<overlapp_counter/(rep*4)<<std::endl;

        // Write results to the output file
        if(f<=2){
            outputFile << N << "\t" << tavo/rep << "\t" << tbso/rep  << "\t" << tstdbso/rep << "\t" << tpointo/rep  << std::endl;
        }
        else{
            outputFile << N << "\t" << 0 << "\t" << tbso/rep  << "\t" << tstdbso/rep << "\t" << tpointo/rep  << std::endl;
        }

    }

    outputFile.close();

    return 0;
}
