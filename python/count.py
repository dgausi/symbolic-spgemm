def count_word_in_file(file_path, word):
    count = 0
    with open(file_path, 'r') as file:
        for line in file:
            count += line.count(word)
    return count

file_path = '../build/meson-logs/testlog.txt'  # Replace with your file path

word_to_count_1 = 'BCast'  # Replace with the word you want to count
word_to_count_2 = 'Simple'  # Replace with the word you want to count
word_to_count_3 = 'Optimal'  # Replace with the word you want to count

count = count_word_in_file(file_path, 'BCast')
print(f"The word BCast appears {count} times in the file.")

count = count_word_in_file(file_path, 'Simple')
print(f"The word Simple appears {count} times in the file.")

count = count_word_in_file(file_path, 'Optimal')
print(f"The word Optimal appears {count} times in the file.")


