import matplotlib.pyplot as plt
import numpy as np

# Read data from the benchmark_results.txt file
data = np.loadtxt('cmake-build-debug/benchmark_results.txt')

# Extract columns
N_values = data[:, 0]
avx_durations = data[:, 1]
bs_durations = data[:, 2]
std_bs_durations = data[:, 3]
point_durations=data[:, 4]

# Plotting
plt.plot(N_values, avx_durations, label='AVX')
plt.plot(N_values, bs_durations, label='Binary Search')
plt.plot(N_values, std_bs_durations, label='Std Binary Search')
plt.plot(N_values,point_durations,label='moving_ptr')

#plt.xscale('log')  # Set x-axis to logarithmic scale

# Add labels and title
plt.xlabel('Matrix Dimension (N)')
plt.ylabel('Duration (s)')
plt.title('Benchmark Results')
plt.legend()
plt.show()
